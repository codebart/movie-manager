package manager.movie.moviemanager.linking;

public class WindowsPathSanitizer implements PathSanitizer {

    @Override
    public String sanitizePath(String path) {
        return path.replaceAll("[\"/*:?><|]", " ").trim();
    }

}
