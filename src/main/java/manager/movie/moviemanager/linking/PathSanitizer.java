package manager.movie.moviemanager.linking;

public interface PathSanitizer {

    String sanitizePath(String path);

}
