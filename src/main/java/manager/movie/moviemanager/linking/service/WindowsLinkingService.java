package manager.movie.moviemanager.linking.service;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.PathSanitizer;
import mslinks.ShellLink;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public class WindowsLinkingService implements PathLinkingService {

    private static final String WINDOWS_LINK_EXTENSION = ".lnk";

    private final ApplicationConfiguration applicationConfiguration;
    private final PathSanitizer pathSanitizer;

    public WindowsLinkingService(ApplicationConfiguration applicationConfiguration, PathSanitizer pathSanitizer) {
        this.applicationConfiguration = applicationConfiguration;
        this.pathSanitizer = pathSanitizer;
    }

    @Override
    public void link(String moviePath, String linkName, String... directories) {
        Path basePath = Paths.get(applicationConfiguration.getLinksDirectory(), directories);
        String linkPath = Paths.get(basePath.toString(), pathSanitizer.sanitizePath(linkName) + WINDOWS_LINK_EXTENSION).toString();
        try {
            log.info("Linking [{}] -> [{}]", linkPath, moviePath);
            ShellLink.createLink(moviePath).saveTo(linkPath);
        } catch (IOException e) {
            throw new LinkerException(String.format("Could not create a link %s -> %s", linkPath, moviePath), e);
        }
    }

}
