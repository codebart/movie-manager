package manager.movie.moviemanager.linking.service;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.linkers.MovieLinker;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.repository.MovieDirectoryRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
@Service
public class MovieLinkingService {

    private final ApplicationConfiguration configuration;
    private final List<MovieLinker> linkers;
    private final MovieDirectoryRepository movieDirectoryRepository;

    @Autowired
    public MovieLinkingService(ApplicationConfiguration configuration, List<MovieLinker> linkers, MovieDirectoryRepository movieDirectoryRepository) {
        this.configuration = configuration;
        this.linkers = linkers;
        this.movieDirectoryRepository = movieDirectoryRepository;
    }

    @Async
    public void linkAll() {
        File baseDirectory = new File(this.configuration.getLinksDirectory());
        if (baseDirectory.exists()) {
            log.info("Deleting directory");
            try {
                FileUtils.deleteDirectory(baseDirectory);
            } catch (IOException e) {
                throw new LinkerException("Could not delete base directory", e);
            }
        }
        log.info("Creating directory");
        if (!baseDirectory.mkdirs()) {
            throw new LinkerException("Base directory was not created");
        }
        log.info("Linking");
        movieDirectoryRepository.getAllMoviesWithDirectories()
                .forEach(movieAndDirectory -> linkMovie(movieAndDirectory.getMovie(), movieAndDirectory.getPath()));
    }

    public void linkMovie(Movie movie, String path) {
        linkers.forEach(linker -> {
            File linkerDirectory = Paths.get(this.configuration.getLinksDirectory(), linker.getName()).toFile();
            log.debug("Creating linker directory: {}", linkerDirectory);
            if (!linkerDirectory.exists() && !linkerDirectory.mkdirs()) {
                throw new LinkerException("Could not get linker directory");
            }
            log.debug("Linking directory: {}", movie);
            linker.createLink(movie, path);
        });
    }

}
