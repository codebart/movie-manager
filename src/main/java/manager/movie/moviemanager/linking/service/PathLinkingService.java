package manager.movie.moviemanager.linking.service;

public interface PathLinkingService {

    void link(String moviePath, String linkName, String... directories);

}
