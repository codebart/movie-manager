package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.model.Person;
import manager.movie.moviemanager.persistence.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Collectors;

@Slf4j
@Component
public class CastLinker implements MovieLinker {

    private final ApplicationConfiguration applicationConfiguration;
    private final PathLinkingService pathLinkingService;
    private final PersonRepository personRepository;

    @Autowired
    public CastLinker(ApplicationConfiguration applicationConfiguration, PathLinkingService pathLinkingService, PersonRepository personRepository) {
        this.applicationConfiguration = applicationConfiguration;
        this.pathLinkingService = pathLinkingService;
        this.personRepository = personRepository;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        String cast = personRepository.getMovieActors(movie.getId()).stream()
                .map(Person::getName)
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.joining(", "));

        File directory = Paths.get(applicationConfiguration.getLinksDirectory(), getName(), cast).toFile();
        if (!directory.exists() && !directory.mkdirs()) {
            throw new LinkerException("Could not get cast " + cast + " directory");
        }
        pathLinkingService.link(path, String.format("%s (%d)", movie.getName(), movie.getYear()), getName(), cast);
    }

    @Override
    public String getName() {
        return "By cast";
    }

}
