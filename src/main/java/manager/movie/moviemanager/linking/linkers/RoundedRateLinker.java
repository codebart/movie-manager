package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.model.Rate;
import manager.movie.moviemanager.persistence.repository.RateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;

@Slf4j
@Component
public class RoundedRateLinker implements MovieLinker {

    private final ApplicationConfiguration applicationConfiguration;
    private final PathLinkingService pathLinkingService;
    private final RateRepository rateRepository;

    @Autowired
    public RoundedRateLinker(ApplicationConfiguration applicationConfiguration, PathLinkingService pathLinkingService, RateRepository rateRepository) {
        this.applicationConfiguration = applicationConfiguration;
        this.pathLinkingService = pathLinkingService;
        this.rateRepository = rateRepository;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        rateRepository.getMovieRate(movie.getId()).map(Rate::getRate).filter(e -> !e.equals(Rate.NONE)).map(Double::valueOf).map(Math::round).ifPresent(rate -> {
            File directory = Paths.get(applicationConfiguration.getLinksDirectory(), getName(), String.valueOf(rate)).toFile();
            if (!directory.exists() && !directory.mkdirs()) {
                throw new LinkerException("Could not get rate " + rate + " directory");
            }
            pathLinkingService.link(path, String.format("%s (%d)", movie.getName(), movie.getYear()), getName(), String.valueOf(rate));
        });
    }

    @Override
    public String getName() {
        return "By rate (rounded)";
    }

}
