package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;

@Slf4j
@Component
public class DirectorLinker implements MovieLinker {

    private final PersonRepository personRepository;
    private final ApplicationConfiguration applicationConfiguration;
    private final PathLinkingService pathLinkingService;

    @Autowired
    public DirectorLinker(PathLinkingService pathLinkingService, PersonRepository personRepository, ApplicationConfiguration applicationConfiguration) {
        this.pathLinkingService = pathLinkingService;
        this.personRepository = personRepository;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        personRepository.getMovieDirectors(movie.getId()).forEach(director -> {
            File directory = Paths.get(applicationConfiguration.getLinksDirectory(), getName(), director.getName()).toFile();
            if (!directory.exists() && !directory.mkdirs()) {
                throw new LinkerException("Could not get director " + director.getName() + " directory");
            }
            pathLinkingService.link(path, String.format("%s (%d)", movie.getName(), movie.getYear()), getName(), director.getName());
        });
    }

    @Override
    public String getName() {
        return "By director";
    }

}
