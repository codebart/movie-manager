package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;

@Slf4j
@Component
public class YearLinker implements MovieLinker {

    private final ApplicationConfiguration applicationConfiguration;
    private final PathLinkingService pathLinkingService;

    @Autowired
    public YearLinker(ApplicationConfiguration applicationConfiguration, PathLinkingService pathLinkingService) {
        this.applicationConfiguration = applicationConfiguration;
        this.pathLinkingService = pathLinkingService;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        File directory = Paths.get(applicationConfiguration.getLinksDirectory(), getName(), movie.getYear().toString()).toFile();
        if (!directory.exists() && !directory.mkdirs()) {
            throw new LinkerException("Could not get year " + movie.getYear() + " directory");
        }
        pathLinkingService.link(path, movie.getName(), getName(), movie.getYear().toString());
    }

    @Override
    public String getName() {
        return "By year";
    }

}
