package manager.movie.moviemanager.linking.linkers;

import manager.movie.moviemanager.persistence.model.Movie;

public interface MovieLinker {

    void createLink(Movie movie, String path);

    String getName();

}
