package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AllMoviesLinker implements MovieLinker {

    private final PathLinkingService pathLinkingService;

    @Autowired
    public AllMoviesLinker(PathLinkingService pathLinkingService) {
        this.pathLinkingService = pathLinkingService;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        pathLinkingService.link(path, String.format("%s (%d)", movie.getName(), movie.getYear()), getName());
    }

    @Override
    public String getName() {
        return "All movies";
    }

}
