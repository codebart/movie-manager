package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Genre;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AllGenresLinker implements MovieLinker {

    private final ApplicationConfiguration applicationConfiguration;
    private final PathLinkingService pathLinkingService;
    private final GenreRepository genreRepository;

    @Autowired
    public AllGenresLinker(ApplicationConfiguration applicationConfiguration, PathLinkingService pathLinkingService, GenreRepository genreRepository) {
        this.applicationConfiguration = applicationConfiguration;
        this.pathLinkingService = pathLinkingService;
        this.genreRepository = genreRepository;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        String genres = genreRepository.findMovieGenres(movie.getId()).stream()
                .map(Genre::getName)
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.joining(", "));

        File directory = Paths.get(applicationConfiguration.getLinksDirectory(), getName(), genres).toFile();
        if (!directory.exists() && !directory.mkdirs()) {
            throw new LinkerException("Could not get genre " + genres + " directory");
        }
        pathLinkingService.link(path, String.format("%s (%d)", movie.getName(), movie.getYear()), getName(), genres);
    }

    @Override
    public String getName() {
        return "By all genres";
    }

}
