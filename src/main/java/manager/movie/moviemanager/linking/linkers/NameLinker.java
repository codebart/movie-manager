package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;

@Slf4j
@Component
public class NameLinker implements MovieLinker {

    private final PathLinkingService pathLinkingService;
    private final ApplicationConfiguration applicationConfiguration;

    @Autowired
    public NameLinker(PathLinkingService pathLinkingService, ApplicationConfiguration applicationConfiguration) {
        this.pathLinkingService = pathLinkingService;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        String letter = movie.getName().substring(0, 1).toUpperCase();
        File directory = Paths.get(applicationConfiguration.getLinksDirectory(), getName(), letter).toFile();
        if (!directory.exists() && !directory.mkdirs()) {
          throw new LinkerException("Could not get rate " + letter + " directory");
        }
        pathLinkingService.link(path, String.format("%s (%d)", movie.getName(), movie.getYear()), getName(), letter);
    }

    @Override
    public String getName() {
        return "By name";
    }

}
