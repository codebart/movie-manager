package manager.movie.moviemanager.linking.linkers;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.LinkerException;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.repository.RateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Paths;

@Slf4j
@Component
public class RateLinker implements MovieLinker {

    private final ApplicationConfiguration applicationConfiguration;
    private final PathLinkingService pathLinkingService;
    private final RateRepository rateRepository;

    @Autowired
    public RateLinker(ApplicationConfiguration applicationConfiguration, PathLinkingService pathLinkingService, RateRepository rateRepository) {
        this.applicationConfiguration = applicationConfiguration;
        this.pathLinkingService = pathLinkingService;
        this.rateRepository = rateRepository;
    }

    @Override
    public void createLink(Movie movie, String path) {
        log.debug("Linking: {}", movie);
        rateRepository.getMovieRate(movie.getId()).ifPresent(rate -> {
            File directory = Paths.get(applicationConfiguration.getLinksDirectory(), getName(), rate.getRate()).toFile();
            if (!directory.exists() && !directory.mkdirs()) {
                throw new LinkerException("Could not get rate " + rate.getRate() + " directory");
            }
            pathLinkingService.link(path, String.format("%s (%d)", movie.getName(), movie.getYear()), getName(), rate.getRate());
        });
    }

    @Override
    public String getName() {
        return "By rate";
    }

}
