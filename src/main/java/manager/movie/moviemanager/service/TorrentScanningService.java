package manager.movie.moviemanager.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import manager.movie.moviemanager.scanner.torrent.TorrentProvider;
import manager.movie.moviemanager.scanner.torrent.TorrentScanner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class TorrentScanningService {

    private final TorrentScanner scanners;

    public List<Torrent> scan(List<TorrentProvider> torrentProviders, int yearFrom, int yearTo, int maxPages) {
        return torrentProviders.stream()
                .flatMap(provider -> {
                    log.info("Scanning torrents with torrent provider: {}", provider);
                    List<Torrent> scan = scanners.scan(provider, yearFrom, yearTo, maxPages);
                    log.info("Found {} new torrents", scan.size());
                    log.debug("Torrents found: {}", scan);
                    return scan.stream();
                })
                .collect(Collectors.toList());
    }

}
