package manager.movie.moviemanager.service;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.persistence.model.Rate;
import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import manager.movie.moviemanager.persistence.repository.RateRepository;
import manager.movie.moviemanager.persistence.repository.torrent.TorrentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ConditionalOnExpression(value = "T(java.awt.Desktop).isDesktopSupported()")
@Component
public class TorrentDownloadService {

    private final TorrentRepository torrentRepository;
    private final RateRepository rateRepository;
    private final Desktop desktop;

    @Autowired
    public TorrentDownloadService(TorrentRepository torrentRepository, RateRepository rateRepository, Desktop desktop) {
        this.torrentRepository = torrentRepository;
        this.rateRepository = rateRepository;
        this.desktop = desktop;
    }

    @Transactional
    public List<Torrent> download(List<String> genres, int minSeeds, int maxCount, double minRate) {
        List<Torrent> torrents = torrentRepository.findAllByDownloadedIsFalse().stream()
                .filter(e -> e.getSeeds() >= minSeeds)
                .filter(e -> genres.stream().anyMatch(g -> torrentRepository.hasTorrentMovieGenre(e.getId(), g)))
                .filter(e -> rateRepository.getMovieRate(e.getMovie().getId()).map(rate -> rate.getRate().equals(Rate.NONE) ? "0" : rate.getRate()).map(Double::valueOf).orElse(0d) > minRate)
                .limit(maxCount)
                .collect(Collectors.toList());

        for (Torrent torrent : torrents) {
            downloadTorrent(torrent);
        }
        return torrents;
    }

    private void downloadTorrent(Torrent torrent) {
        log.info("Downloading: {}", torrent);
        try {
            desktop.browse(URI.create(torrent.getMagnet()));
            torrent.setDownloaded(true);
            torrentRepository.save(torrent);
        } catch (IOException e) {
            log.error("Error while downloading torrent", e);
        }
    }

}
