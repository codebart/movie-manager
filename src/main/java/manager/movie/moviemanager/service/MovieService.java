package manager.movie.moviemanager.service;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.linking.service.MovieLinkingService;
import manager.movie.moviemanager.persistence.model.MovieDirectory;
import manager.movie.moviemanager.scanner.directory.DirectoryScannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MovieService {

    private final ApplicationConfiguration applicationConfiguration;
    private final DirectoryScannerService directoryScannerService;
    private final MovieLinkingService movieLinkingService;

    @Autowired
    public MovieService(ApplicationConfiguration applicationConfiguration, DirectoryScannerService directoryScannerService, MovieLinkingService movieLinkingService) {
        this.applicationConfiguration = applicationConfiguration;
        this.directoryScannerService = directoryScannerService;
        this.movieLinkingService = movieLinkingService;
    }

    public List<MovieDirectory> scan() {
        log.info("Scanning directories: {}", applicationConfiguration.getSearchDirectories());
        List<MovieDirectory> movies = applicationConfiguration.getSearchDirectories().stream()
                .map(File::new)
                .map(directoryScannerService::scanDirectory)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        log.info("Found {} new movie directories", movies.size());
        if (!new File(applicationConfiguration.getLinksDirectory()).exists()) {
            movieLinkingService.linkAll();
        } else if (!movies.isEmpty()) {
            movies.forEach(movieDirectory -> movieLinkingService.linkMovie(movieDirectory.getMovie(), movieDirectory.getPath()));
        }
        return movies;
    }

}
