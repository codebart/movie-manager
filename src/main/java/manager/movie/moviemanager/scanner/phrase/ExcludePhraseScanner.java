package manager.movie.moviemanager.scanner.phrase;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.scanner.MovieNameScanner;
import manager.movie.moviemanager.scanner.MovieYearScanner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@AllArgsConstructor
public class ExcludePhraseScanner implements MovieNameScanner {

    private final List<ReplacementRule> replacementRules;
    private final MovieYearScanner movieYearScanner;

    @Override
    public Optional<String> scanName(String input) {
        Optional<Integer> year = movieYearScanner.scanYear(input);
        if (!year.isPresent()) {
            return Optional.empty();
        }
        int intYear = year.orElseThrow(IllegalStateException::new);
        String name = input.replaceAll(String.valueOf(intYear) + ".*", "")
                .toLowerCase();

        for (ReplacementRule excludedPhrase : replacementRules) {
            name = excludedPhrase.replaced(name);
        }

        if (name.contains(String.valueOf(intYear))) {
            name = name.replaceAll(String.valueOf(intYear), "");
        }

        if (name.equalsIgnoreCase(input)) {
            return Optional.empty();
        }
        return Optional.of(name);
    }

}
