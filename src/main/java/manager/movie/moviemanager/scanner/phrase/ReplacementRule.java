package manager.movie.moviemanager.scanner.phrase;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class ReplacementRule {

    private final String regex;
    private final String replacement;

    public ReplacementRule(String regex) {
        this(regex, "");
    }

    public String replaced(String input) {
        return input.replaceAll(regex, replacement)
                .replaceAll(" {2,}", " ")
                .trim();
    }

}
