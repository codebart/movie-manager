package manager.movie.moviemanager.scanner;

import java.util.Optional;

public interface MovieNameScanner {

    Optional<String> scanName(String input);

}
