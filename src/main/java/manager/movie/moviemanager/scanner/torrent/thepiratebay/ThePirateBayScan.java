package manager.movie.moviemanager.scanner.torrent.thepiratebay;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.TorrentConfiguration;
import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import manager.movie.moviemanager.persistence.repository.torrent.TorrentRepository;
import manager.movie.moviemanager.persistence.repository.torrent.TorrentScanRepository;
import manager.movie.moviemanager.scanner.torrent.TorrentFilter;
import manager.movie.moviemanager.scanner.torrent.TorrentProvider;
import manager.movie.moviemanager.scanner.torrent.TorrentScan;
import manager.movie.moviemanager.scanner.torrent.TorrentUtils;
import manager.movie.moviemanager.utils.TimerUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.util.IterableUtils;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ThePirateBayScan implements TorrentScan {

    private static final String PIRATE_BAY_LINK = "https://thepiratebay.org/search/%s %s/%s/7/200";
    private static final Pattern SIZE_PATTERN = Pattern.compile("Size (.*),");

    private final TorrentConfiguration torrentConfiguration;
    private final TorrentRepository torrentRepository;
    private final TorrentScanRepository torrentScanRepository;
    private final TorrentFilter torrentFilter;

    @Autowired
    public ThePirateBayScan(TorrentConfiguration torrentConfiguration, TorrentRepository torrentRepository, TorrentScanRepository torrentScanRepository, TorrentFilter torrentFilter) {
        this.torrentConfiguration = torrentConfiguration;
        this.torrentRepository = torrentRepository;
        this.torrentScanRepository = torrentScanRepository;
        this.torrentFilter = torrentFilter;
    }

    @Override
    public TorrentProvider provider() {
        return TorrentProvider.THE_PIRATE_BAY;
    }

    @Override
    public int pages(int year) {
        try {
            Document document = requestPirateBay(year, 0);
            int pages = document.body()
                    .getElementById("SearchResults")
                    .getElementById("content")
                    .getElementsByAttributeValue("align", "center")
                    .get(0)
                    .getElementsByTag("a")
                    .stream()
                    .map(Element::html)
                    .map(e -> {
                        try {
                            return Integer.valueOf(e);
                        } catch (Throwable ex) {
                            return 0;
                        }
                    })
                    .filter(e -> e > 0)
                    .max(Comparator.naturalOrder())
                    .orElse(0);
            log.info("Found pages count: {}", pages);
            if (pages >= torrentConfiguration.getPageLimit()) {
                return torrentConfiguration.getPageLimit();
            }
            return pages;
        } catch (Throwable e) {
            log.error("Error while requesting ThePirateBay");
            return 0;
        }
    }

    private Document requestPirateBay(int year, int page) {
        String link = String.format(PIRATE_BAY_LINK, year, torrentConfiguration.getTorrentQuality().getName(), page);
        log.debug("Requesting: {}", link);
        return TimerUtils.measured(() -> Jsoup.connect(link).timeout(torrentConfiguration.getRequestTimeout()).get());
    }

    @Override
    public List<Torrent> torrents(int year, int page) {
        log.info("Scanning page: {}", page);
        String link = String.format(PIRATE_BAY_LINK, year, torrentConfiguration.getTorrentQuality().getName(), page);
        manager.movie.moviemanager.persistence.model.torrent.TorrentScan torrentScan = manager.movie.moviemanager.persistence.model.torrent.TorrentScan.builder()
                .year(year)
                .link(link)
                .page(page)
                .provider(provider())
                .quality(torrentConfiguration.getTorrentQuality())
                .build();
        try {
            Document document = requestPirateBay(year, page);

            List<Torrent> torrents = getTorrents(torrentScan, document, year).stream()
                    .filter(torrentFilter::filterTorrent)
                    .collect(Collectors.toList());

            log.info("Found {} torrents: {}", torrents.size(), torrents);
            torrentScanRepository.save(torrentScan);
            return IterableUtils.toList(torrentRepository.saveAll(torrents));
        } catch (Throwable e) {
            log.error("Request error");
            return Collections.emptyList();
        }
    }

    private List<Torrent> getTorrents(manager.movie.moviemanager.persistence.model.torrent.TorrentScan scan, Document document, int year) {
        Elements links = getLinks(document);
        log.info("Found {} torrent links", links.size());
        return links.stream()
                .map(e -> {
                    Element main = getTd(e, 1);
                    int seeds = Integer.valueOf(getTd(e, 2).html());
                    int leeches = Integer.valueOf(getTd(e, 3).html());

                    Element nameElement = main.getElementsByClass("detName").get(0).getElementsByClass("detLink").get(0);
                    String name = nameElement.html();
                    String link = nameElement.attr("href");
                    String magnet = main.getElementsByTag("a").stream().filter(a -> a.attr("href").contains("magnet")).findFirst().orElseThrow(IllegalStateException::new).attr("href");
                    String details = main.getElementsByClass("detDesc").html();

                    return Torrent.builder()
                            .details(details)
                            .leech(leeches)
                            .seeds(seeds)
                            .quality(torrentConfiguration.getTorrentQuality())
                            .provider(provider())
                            .scan(scan)
                            .name(name)
                            .link(link)
                            .magnet(magnet)
                            .year(year)
                            .originalSize(findSize(details))
                            .sizeInMb(TorrentUtils.getSizeInMb(findSize(details)).orElse(null))
                            .build();
                })
                .peek(torrent -> log.debug("Found torrent: {}", torrent))
                .collect(Collectors.toList());
    }

    private String findSize(String description) {
        Matcher matcher = SIZE_PATTERN.matcher(description);
        if (matcher.find()) {
            return matcher.group(1).replaceAll("&nbsp;", "");
        }
        return null;
    }

    private Element getTd(Element element, int ix) {
        return element.getElementsByTag("td").get(ix);
    }

    private Elements getLinks(Document document) {
        return document
                .body()
                .getElementById("SearchResults")
                .getElementById("content")
                .getElementById("main-content")
                .getElementById("searchResult")
                .getElementsByTag("tbody")
                .get(0)
                .getElementsByTag("tr");
    }

}
