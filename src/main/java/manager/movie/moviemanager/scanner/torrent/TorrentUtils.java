package manager.movie.moviemanager.scanner.torrent;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@UtilityClass
public class TorrentUtils {

    private final Pattern SIZE_MB_PATTERN = Pattern.compile("([0-9]+(\\.[0-9]+)?).*");
    private final String GIGA_PREFIX = "GiB|GB";
    private final String MEGA_PREFIX = "MiB|MB";

    public Optional<Integer> getSizeInMb(String input) {
        if (input == null || !input.matches(sizeRegex(GIGA_PREFIX, MEGA_PREFIX))) {
            return Optional.empty();
        }

        int multiplier = input.matches(sizeRegex(GIGA_PREFIX)) ? 1024 : 1;
        Matcher matcher = SIZE_MB_PATTERN.matcher(input);

        if (matcher.find()) {
            return Optional.of((int) (Double.valueOf(matcher.group(1)) * multiplier));
        }

        return Optional.empty();
    }

    private String sizeRegex(String... prefix) {
        return Arrays.stream(prefix).collect(Collectors.joining("|", ".*(", ").*"));
    }

}
