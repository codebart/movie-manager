package manager.movie.moviemanager.scanner.torrent.x1337;

import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.TorrentConfiguration;
import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import manager.movie.moviemanager.persistence.repository.torrent.TorrentRepository;
import manager.movie.moviemanager.persistence.repository.torrent.TorrentScanRepository;
import manager.movie.moviemanager.scanner.torrent.TorrentFilter;
import manager.movie.moviemanager.scanner.torrent.TorrentProvider;
import manager.movie.moviemanager.scanner.torrent.TorrentScan;
import manager.movie.moviemanager.scanner.torrent.TorrentUtils;
import manager.movie.moviemanager.utils.TimerUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.util.IterableUtils;
import org.springframework.stereotype.Component;

import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Component
public class X1337Scan implements TorrentScan {

    private static final String X_1337_URL_ROOT = "https://1337x.to";
    private static final String X_1337_URL = X_1337_URL_ROOT + "/search/%d+%s/%d/";
    private static final Pattern HREF_PATTERN = Pattern.compile("/search/(.*)/(.*)/");

    private final TorrentConfiguration torrentConfiguration;
    private final TorrentRepository torrentRepository;
    private final TorrentFilter torrentFilter;
    private final TorrentScanRepository torrentScanRepository;

    @Autowired
    public X1337Scan(TorrentConfiguration torrentConfiguration, TorrentRepository torrentRepository, TorrentScanRepository torrentScanRepository, TorrentFilter torrentFilter) {
        this.torrentConfiguration = torrentConfiguration;
        this.torrentRepository = torrentRepository;
        this.torrentScanRepository = torrentScanRepository;
        this.torrentFilter = torrentFilter;
    }

    @Override
    public TorrentProvider provider() {
        return TorrentProvider.X_1337;
    }

    @Override
    public List<Torrent> torrents(int year, int page) {
        try {
            manager.movie.moviemanager.persistence.model.torrent.TorrentScan scan = manager.movie.moviemanager.persistence.model.torrent.TorrentScan.builder()
                    .link("")
                    .page(page)
                    .year(year)
                    .provider(provider())
                    .quality(torrentConfiguration.getTorrentQuality())
                    .build();
            Document document = requestX1337(year, page);
            List<Torrent> torrents = getBoxInfoDetail(document)
                    .getElementsByClass("table-list-wrap")
                    .first()
                    .getElementsByTag("table")
                    .first()
                    .getElementsByTag("tbody")
                    .first()
                    .getElementsByTag("tr")
                    .stream()
                    .map(e -> requestTorrent(scan, e, year))
                    .filter(Objects::nonNull)
                    .filter(torrentFilter::filterTorrent)
                    .collect(Collectors.toList());
            log.info("Found {} torrents: {}", torrents.size(), torrents);
            torrentScanRepository.save(scan);
            return IterableUtils.toList(torrentRepository.saveAll(torrents));
        } catch (UncheckedIOException e) {
            log.error("Request error");
            return Collections.emptyList();
        }
    }

    private Torrent requestTorrent(manager.movie.moviemanager.persistence.model.torrent.TorrentScan scan, Element element, int year) {
        try {
            String name = element.getElementsByTag("a").get(1).text();
            String url = X_1337_URL_ROOT + element.getElementsByTag("a").get(1).attr("href");
            int seeds = Integer.valueOf(element.getElementsByClass("seeds").html().replaceAll("\n.*", ""));
            int leech = Integer.valueOf(element.getElementsByClass("leeches").html().replaceAll("\n.*", ""));
            String details = element.getElementsByClass("coll-5")
                    .first()
                    .getElementsByTag("a")
                    .first()
                    .html();
            String size = element.getElementsByClass("size")
                    .first()
                    .text();

            log.debug("Requesting: {}", url);
            Document document = TimerUtils.measured(() -> Jsoup.connect(url).timeout(torrentConfiguration.getRequestTimeout()).get());
            String magnet = document.body()
                    .getElementsByTag("main")
                    .first()
                    .getElementsByClass("row")
                    .first()
                    .getElementsByClass("page-content")
                    .first()
                    .getElementsByClass("box-info")
                    .first()
                    .getElementsByClass("box-info-detail")
                    .first()
                    .getElementsByClass("torrent-category-detail")
                    .first()
                    .getElementsByTag("ul")
                    .first()
                    .getElementsByTag("li")
                    .first()
                    .getElementsByTag("a")
                    .first()
                    .attr("href");

            return Torrent.builder()
                    .scan(scan)
                    .name(name)
                    .year(year)
                    .magnet(magnet)
                    .provider(provider())
                    .quality(torrentConfiguration.getTorrentQuality())
                    .details(details)
                    .seeds(seeds)
                    .leech(leech)
                    .link(url)
                    .originalSize(size)
                    .sizeInMb(TorrentUtils.getSizeInMb(size).orElse(null))
                    .build();
        } catch (Throwable e) {
            log.error("Could not parse torrent", e);
            return null;
        }
    }

    @Override
    public int pages(int year) {
        try {
            Document document = requestX1337(year, 0);

            String href = getBoxInfoDetail(document)
                    .getElementsByClass("pagination")
                    .first()
                    .getElementsByTag("ul")
                    .first()
                    .getElementsByClass("last")
                    .first()
                    .getElementsByTag("a")
                    .first()
                    .attr("href");

            Matcher matcher = HREF_PATTERN.matcher(href);
            if (!matcher.find()) {
                return 1;
            }

            return Integer.valueOf(matcher.group(2));
        } catch (UncheckedIOException e) {
            log.error("Request error");
            return 1;
        } catch (NumberFormatException e) {
            log.error("Format error", e);
            return 1;
        }
    }

    private Element getBoxInfoDetail(Document document) {
        return document.body()
                .getElementsByTag("main")
                .first()
                .getElementsByClass("row")
                .first()
                .getElementsByClass("search-page")
                .first()
                .getElementsByClass("box-info")
                .first()
                .getElementsByClass("box-info-detail")
                .first();
    }

    private Document requestX1337(int year, int page) {
        String link = String.format(X_1337_URL, year, torrentConfiguration.getTorrentQuality().getName(), page);
        log.debug("Requesting: {}", link);
        return TimerUtils.measured(() -> Jsoup.connect(link).timeout(torrentConfiguration.getRequestTimeout()).get());
    }


}
