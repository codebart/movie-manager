package manager.movie.moviemanager.scanner.torrent;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TorrentQuality {
    HD("720p", 720),
    FULL_HD("1080p", 1080),
    ULTRA_HD("4k", 2160);

    private final String name;
    private final int resolution;

}
