package manager.movie.moviemanager.scanner.torrent;

import manager.movie.moviemanager.persistence.model.torrent.Torrent;

import java.util.List;

public interface TorrentScan {

    TorrentProvider provider();

    List<Torrent> torrents(int year, int page);

    int pages(int year);

}
