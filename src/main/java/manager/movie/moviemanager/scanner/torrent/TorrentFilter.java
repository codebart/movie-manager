package manager.movie.moviemanager.scanner.torrent;

import manager.movie.moviemanager.configuration.TorrentConfiguration;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import manager.movie.moviemanager.persistence.repository.MovieDirectoryRepository;
import manager.movie.moviemanager.persistence.repository.torrent.TorrentRepository;
import manager.movie.moviemanager.scanner.MovieNameScanner;
import manager.movie.moviemanager.scanner.MovieYearScanner;
import manager.movie.moviemanager.scanner.database.MovieDatabaseScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TorrentFilter {

    private final TorrentConfiguration torrentConfiguration;
    private final MovieNameScanner movieNameScanner;
    private final MovieYearScanner movieYearScanner;
    private final MovieDatabaseScanner movieDatabaseScanner;
    private final TorrentRepository torrentRepository;
    private final MovieDirectoryRepository movieDirectoryRepository;

    @Autowired
    public TorrentFilter(TorrentConfiguration torrentConfiguration, MovieNameScanner movieNameScanner, MovieYearScanner movieYearScanner, MovieDatabaseScanner movieDatabaseScanner, TorrentRepository torrentRepository, MovieDirectoryRepository movieDirectoryRepository) {
        this.torrentConfiguration = torrentConfiguration;
        this.movieNameScanner = movieNameScanner;
        this.movieYearScanner = movieYearScanner;
        this.movieDatabaseScanner = movieDatabaseScanner;
        this.torrentRepository = torrentRepository;
        this.movieDirectoryRepository = movieDirectoryRepository;
    }

    public boolean filterTorrent(Torrent torrent) {
        if (torrent.getSeeds() < torrentConfiguration.getMinSeeds()
                || torrent.getSizeInMb() == null
                || torrent.getSizeInMb() < torrentConfiguration.getSizeFrom()
                || torrent.getSizeInMb() > torrentConfiguration.getSizeTo()) {
            return false;
        }

        Integer year = movieYearScanner.scanYear(torrent.getName()).orElse(null);
        String name = movieNameScanner.scanName(torrent.getName()).orElse(null);
        if (year == null || name == null) {
            return false;
        }

        Movie movie = movieDatabaseScanner.findMovie(name, year).orElse(null);
        if (movie == null || torrentRepository.existsByMovie(movie.getId()) || movieDirectoryRepository.existsByMovie(movie.getId())) {
            return false;
        }

        torrent.setMovie(movie);

        return true;
    }

}
