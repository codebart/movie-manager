package manager.movie.moviemanager.scanner.torrent;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.TorrentConfiguration;
import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import manager.movie.moviemanager.persistence.repository.torrent.TorrentScanRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@AllArgsConstructor
@Component
public class TorrentScanner {

    private final TorrentConfiguration torrentConfiguration;
    private final TorrentScanRepository torrentScanRepository;
    private final List<TorrentScan> torrentScans;

    public List<Torrent> scan(TorrentProvider provider, int yearFrom, int yearTo, int maxPages) {
        return IntStream.rangeClosed(yearFrom, yearTo)
                .mapToObj(year -> scan(provider, year, maxPages))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public List<Torrent> scan(TorrentProvider provider, int year, int maxPages) {
        TorrentScan scan = scan(provider);
        log.info("Scanning year: {}", year);
        int pages = scan.pages(year);
        log.info("Pages found: {}", pages);
        List<Torrent> torrents = new ArrayList<>();
        for (int page = 0; page < (pages < maxPages ? pages : maxPages); page++) {
            log.info("Scanning page: {}", page);
            if (!torrentScanRepository.scanExists(year, page, scan.provider(), torrentConfiguration.getTorrentQuality())) {
                List<Torrent> found = scan.torrents(year, page);
                torrents.addAll(found);
                if (found.isEmpty()) {
                    log.info("No more torrents found for year: {}", year);
                    break;
                }
            }
        }
        return torrents;
    }

    private TorrentScan scan(TorrentProvider provider) {
        return torrentScans.stream()
                .filter(e -> e.provider().equals(provider))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
