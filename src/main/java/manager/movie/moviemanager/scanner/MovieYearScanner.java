package manager.movie.moviemanager.scanner;

import java.time.LocalDate;
import java.util.Optional;

public interface MovieYearScanner {

    int YEAR_MIN = 1900;
    int YEAR_MAX = LocalDate.now().getYear();

    Optional<Integer> scanYear(String input);

}
