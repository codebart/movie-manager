package manager.movie.moviemanager.scanner.directory;


import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.ApplicationConfiguration;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.model.MovieDirectory;
import manager.movie.moviemanager.persistence.repository.MovieDirectoryRepository;
import manager.movie.moviemanager.scanner.MovieNameScanner;
import manager.movie.moviemanager.scanner.MovieYearScanner;
import manager.movie.moviemanager.scanner.ScannerException;
import manager.movie.moviemanager.scanner.database.MovieDatabaseScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DirectoryScannerService {

    private final ApplicationConfiguration applicationConfiguration;
    private final MovieNameScanner movieNameScanner;
    private final MovieYearScanner movieYearScanner;
    private final MovieDatabaseScanner movieDatabaseScanner;
    private final MovieDirectoryRepository movieDirectoryRepository;

    @Autowired
    public DirectoryScannerService(ApplicationConfiguration applicationConfiguration, MovieNameScanner movieNameScanner, MovieYearScanner movieYearScanner, MovieDatabaseScanner movieDatabaseScanner, MovieDirectoryRepository movieDirectoryRepository) {
        this.applicationConfiguration = applicationConfiguration;
        this.movieNameScanner = movieNameScanner;
        this.movieYearScanner = movieYearScanner;
        this.movieDatabaseScanner = movieDatabaseScanner;
        this.movieDirectoryRepository = movieDirectoryRepository;
    }

    public List<MovieDirectory> scanDirectory(File file) {
        log.info("Scanning root directory: {}", file);
        try {
            return Files.walk(file.toPath(), applicationConfiguration.getScanDepth())
                    .parallel()
                    .filter(e -> !e.equals(file.toPath()))
                    .filter(e -> e.getFileName() != null)
                    .filter(e -> applicationConfiguration.getIgnoredDirectories().stream().noneMatch(dir -> e.getFileName().toString().contains(dir)))
                    .filter(e -> !movieDirectoryRepository.existsByPath(e.toAbsolutePath().toString()))
                    .collect(Collectors.toList())
                    .parallelStream()
                    .map(this::processDirectory)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new ScannerException("Directory scanning error", e);
        }
    }

    private MovieDirectory processDirectory(Path path) {
        log.debug("Scanning: {}", path);
        Integer year = movieYearScanner.scanYear(path.toString()).orElse(null);
        if (year == null) {
            return null;
        }
        String name = movieNameScanner.scanName(path.getFileName().toString()).orElse(null);
        if (name == null) {
            return null;
        }

        Optional<Movie> foundMovie = movieDatabaseScanner.findMovie(name, year);
        if (!foundMovie.isPresent()) {
            log.warn("Could not find movie for directory: {}", path.toString());
        } else {
            log.info("Found movie directory: [{} ({})] in: [{}]", name, year, path.toString());
        }
        return foundMovie
                .map(movie -> MovieDirectory.builder().movie(movie).path(path.toString()).build())
                .map(movieDirectoryRepository::save)
                .orElse(null);
    }

}
