package manager.movie.moviemanager.scanner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
public class SimpleYearScanner implements MovieYearScanner {

    private static final Pattern BASIC_PATTERN = Pattern.compile(".*?(\\d{4}).*?");
    private static final Pattern BRACKET_PATTERN = Pattern.compile(".*?\\((\\d{4})\\).*?");

    @Override
    public Optional<Integer> scanYear(String input) {
        Integer bracket = scanYear(input, BRACKET_PATTERN);
        if (bracket != null) {
            return Optional.of(bracket);
        }
        Integer basic = scanYear(input, BASIC_PATTERN);
        if (basic != null) {
            return Optional.of(basic);
        }
        return Optional.empty();
    }

    private Integer scanYear(String input, Pattern pattern) {
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                try {
                    int year = Integer.valueOf(matcher.group(i));

                    if (year >= MovieYearScanner.YEAR_MIN && year <= MovieYearScanner.YEAR_MAX) {
                        return year;
                    }
                } catch (NumberFormatException e) {
                    log.warn("Could not parse year: {}", matcher.group(i));
                }
            }
        }

        return null;
    }

}
