package manager.movie.moviemanager.scanner.database.imdb;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ImdbAggregateRating {

    private String ratingValue;

}
