package manager.movie.moviemanager.scanner.database.imdb;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ImdbSuggestionResult {

    @JsonProperty(value = "v")
    private Integer version;

    @JsonProperty(value = "q")
    private String query;

    @JsonProperty(value = "d")
    private List<ImdbSuggestion> suggestions = new ArrayList<>();

}
