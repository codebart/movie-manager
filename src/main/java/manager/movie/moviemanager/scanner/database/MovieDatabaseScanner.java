package manager.movie.moviemanager.scanner.database;


import manager.movie.moviemanager.persistence.model.Movie;

import java.util.Optional;

public interface MovieDatabaseScanner {

    Optional<Movie> findMovie(String name, int year);

}
