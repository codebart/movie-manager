package manager.movie.moviemanager.scanner.database.imdb;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ImdbMovie {

    private String url;
    private String name;
    private List<String> genre = new ArrayList<>();
    private List<ImdbActor> actor = new ArrayList<>();
    private List<ImdbActor> director = new ArrayList<>();
    private ImdbAggregateRating aggregateRating;

}
