package manager.movie.moviemanager.scanner.database.imdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import manager.movie.moviemanager.configuration.OmdbProperties;
import manager.movie.moviemanager.persistence.model.Genre;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.persistence.model.Person;
import manager.movie.moviemanager.persistence.model.Rate;
import manager.movie.moviemanager.persistence.repository.GenreRepository;
import manager.movie.moviemanager.persistence.repository.MovieRepository;
import manager.movie.moviemanager.persistence.repository.PersonRepository;
import manager.movie.moviemanager.persistence.repository.RateRepository;
import manager.movie.moviemanager.scanner.database.MovieDatabaseScanner;
import manager.movie.moviemanager.utils.TimerUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ImdbScanner implements MovieDatabaseScanner {

    private static final String IMDB_URL = "https://www.imdb.com/title/%s/";

    private static final String IMDB_SUGGESTIONS_URL = "http://sg.media-imdb.com/suggests/%s/%s.json";
    private static final String IMDB_SUGGESTIONS_REDUNDANT = "imdb\\$%s\\(";
    private static final int IMDB_SUGGESTIONS_YEAR_SLIP = 2;
    private static final String OMDB_SHORT_URL = "http://www.omdbapi.com/?t=%s&y=%d&apikey=%s";

    private final MovieRepository movieRepository;
    private final ObjectMapper objectMapper;
    private final GenreRepository genreRepository;
    private final PersonRepository personRepository;
    private final RateRepository rateRepository;
    private final OmdbProperties omdbProperties;

    @Autowired
    public ImdbScanner(MovieRepository movieRepository, ObjectMapper objectMapper, GenreRepository genreRepository, PersonRepository personRepository, RateRepository rateRepository, OmdbProperties omdbProperties) {
        this.movieRepository = movieRepository;
        this.objectMapper = objectMapper;
        this.genreRepository = genreRepository;
        this.personRepository = personRepository;
        this.rateRepository = rateRepository;
        this.omdbProperties = omdbProperties;
    }

    @Override
    public Optional<Movie> findMovie(String name, int year) {
        if (movieRepository.existsByExtractedNameAndYear(name, year)) {
            log.debug("Movie {} ({}) already in database", name, year);
            return Optional.of(movieRepository.findByExtractedNameAndYear(name, year));
        }
        log.debug("Searching for {} ({}) on IMDB", name, year);
        // Quick search for suggestions
        String imdbId = getImdbId(name, year);
        if (imdbId == null) {
            return Optional.empty();
        }

        String imdbUrl = String.format(IMDB_URL, imdbId);
        log.debug("Requesting IMDB: {}", imdbUrl);

        try {
            Document document = TimerUtils.measured(() -> Jsoup.connect(imdbUrl).get());

            String json = document.getElementsByTag("script").stream()
                    .filter(e -> e.hasAttr("type"))
                    .filter(e -> e.attr("type").equals("application/ld+json"))
                    .findAny()
                    .orElseThrow(IOException::new)
                    .html();

            ImdbMovie imdbMovie = objectMapper.readValue(json, ImdbMovie.class);

            if (movieRepository.existsByNameAndYear(imdbMovie.getName(), year)) {
                return Optional.ofNullable(movieRepository.findByNameAndYear(imdbMovie.getName(), year));
            }

            Set<Genre> genres = imdbMovie.getGenre().stream()
                    .map(e -> genreRepository.findByName(e).orElseGet(() -> genreRepository.save(new Genre(e))))
                    .collect(Collectors.toSet());

            Set<Person> actors = imdbMovie.getActor().stream()
                    .map(ImdbActor::getName)
                    .map(e -> personRepository.getByName(e).orElseGet(() -> personRepository.save(new Person(e))))
                    .collect(Collectors.toSet());

            Set<Person> directors = imdbMovie.getDirector().stream()
                    .map(ImdbActor::getName)
                    .map(e -> personRepository.getByName(e).orElseGet(() -> personRepository.save(new Person(e))))
                    .collect(Collectors.toSet());

            String rateFind = imdbMovie.getAggregateRating() != null && imdbMovie.getAggregateRating().getRatingValue() != null ? imdbMovie.getAggregateRating().getRatingValue() : Rate.NONE;

            Rate rate = rateRepository.findByRate(rateFind).orElseGet(() -> rateRepository.save(new Rate(rateFind)));

            Movie movie = Movie.builder()
                    .actors(actors)
                    .genres(genres)
                    .imdbId(imdbId)
                    .directors(directors)
                    .rate(rate)
                    .year(year)
                    .name(imdbMovie.getName())
                    .extractedName(name)
                    .build();

            log.info("Added new movie to database: {}", movie);
            return Optional.of(movieRepository.save(movie));
        } catch (Throwable e) {
            log.error("Could not parse IMDB movie json", e);
        }

        return Optional.empty();
    }

    private String getImdbId(String name, int year) {
        ImdbSuggestion suggestion = getMovieSuggestion(name, year);
        if (suggestion != null && suggestion.getImdbId() != null) {
            return suggestion.getImdbId();
        }
        OmdbMovie omdbMovie = getOmdbMovie(name, year);
        if (omdbMovie != null && omdbMovie.getImdbID() != null) {
            return omdbMovie.getImdbID();
        }
        log.debug("Could not find IMDB id for: {} ({})", name, year);
        return null;
    }

    private String getFirstLetter(String input) {
        return Arrays.stream(input.split(""))
                .filter(StringUtils::isAlpha)
                .findFirst()
                .orElse("a");
    }

    private ImdbSuggestion getMovieSuggestion(String name, int year) {
        RestTemplate restTemplate = new RestTemplate();
        String firstLetter = getFirstLetter(name);
        String query = name.replaceAll("[0-9]", "")
                .replaceAll("[^a-zA-Z ]", "")
                .trim()
                .replaceAll(" ", "_");

        if (query.isEmpty()) {
            return null;
        }

        String suggestionUrl = String.format(IMDB_SUGGESTIONS_URL, firstLetter, query);

        try {
            log.debug("Requesting IMDB suggestions: {}", suggestionUrl);
            String response = TimerUtils.measured(() -> restTemplate.getForObject(suggestionUrl, String.class));

            response = response.replaceFirst(String.format(IMDB_SUGGESTIONS_REDUNDANT, query), "");
            response = response.substring(0, response.length() - 1);

            ImdbSuggestionResult suggestionResult = objectMapper.readValue(response, ImdbSuggestionResult.class);
            return suggestionResult.getSuggestions().stream()
                        .filter(e -> e.getYear() != null && e.getYear().contains(String.valueOf(year)))
                        .findAny()
                        // Search for +/- IMDB_SUGGESTIONS_YEAR_SLIP years in case of wrong year in torrent name
                        .orElseGet(() -> suggestionResult.getSuggestions().stream()
                                .filter(e -> e.getYear() != null)
                                .filter(e -> Math.abs(Integer.valueOf(e.getYear()) - year) < IMDB_SUGGESTIONS_YEAR_SLIP)
                                .findFirst()
                                .orElse(null)
                        );
        } catch (IOException e) {
            log.error("Error while parsing suggestions", e);
        } catch (Throwable e) {
            log.error("Error while requesting IMDB suggestions", e);
        }
        return null;
    }

    private OmdbMovie getOmdbMovie(String name, int year) {
        RestTemplate restTemplate = new RestTemplate();

        String omdbUrl = String.format(OMDB_SHORT_URL, name.replaceAll(" ", "+"), year, omdbProperties.getApiKey());

        log.debug("Requesting OMDB: {}", omdbUrl);
        String response = TimerUtils.measured(() -> restTemplate.getForObject(omdbUrl, String.class));

        try {
            return objectMapper.readValue(response, OmdbMovie.class);
        } catch (Throwable e) {
            log.error("Error while requesting OMDB");
        }
        return null;
    }

}
