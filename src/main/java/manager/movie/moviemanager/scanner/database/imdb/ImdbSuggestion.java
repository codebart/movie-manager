package manager.movie.moviemanager.scanner.database.imdb;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ImdbSuggestion {

    @JsonProperty(value = "l")
    private String l;

    @JsonProperty(value = "id")
    private String imdbId;

    @JsonProperty(value = "s")
    private String starring;

    @JsonProperty(value = "y")
    private String year;

    @JsonProperty(value = "q")
    private String movieType;

}
