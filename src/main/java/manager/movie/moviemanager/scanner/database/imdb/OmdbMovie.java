package manager.movie.moviemanager.scanner.database.imdb;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OmdbMovie {

    @JsonProperty("Title")
    private String name;

    @JsonProperty("Year")
    private Integer year;

    private String imdbID;

}
