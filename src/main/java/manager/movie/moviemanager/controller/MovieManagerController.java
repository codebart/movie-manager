package manager.movie.moviemanager.controller;

import lombok.AllArgsConstructor;
import manager.movie.moviemanager.linking.service.MovieLinkingService;
import manager.movie.moviemanager.persistence.model.MovieDirectory;
import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import manager.movie.moviemanager.scanner.torrent.TorrentProvider;
import manager.movie.moviemanager.service.MovieService;
import manager.movie.moviemanager.service.TorrentDownloadService;
import manager.movie.moviemanager.service.TorrentScanningService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
@AllArgsConstructor
public class MovieManagerController {

    private final MovieService movieService;
    private final TorrentDownloadService torrentDownloadService;
    private final TorrentScanningService torrentScanningService;
    private final MovieLinkingService movieLinkingService;

    @PostMapping(path = "/library/scan")
    public List<MovieDirectory> scan() {
        return movieService.scan();
    }

    @PostMapping(path = "/library/link/all")
    public void linkAll() {
        movieLinkingService.linkAll();
    }

    @PostMapping(path = "/torrent/scan")
    public List<Torrent> scanTorrents(@RequestParam(value = "providers") List<TorrentProvider> providers,
                                      @RequestParam(value = "yearFrom") int yearFrom,
                                      @RequestParam(value = "yearTo") int yearTo,
                                      @RequestParam(value = "maxPages", required = false, defaultValue = "1") int maxPages) {
        return torrentScanningService.scan(providers, yearFrom, yearTo, maxPages);
    }

    @PostMapping(path = "/torrent/download")
    public List<Torrent> downloadTorrents(@RequestParam(value = "genres", required = false, defaultValue = "") List<String> genres,
                                          @RequestParam(value = "minSeeds", required = false, defaultValue = "0") int minSeeds,
                                          @RequestParam(value = "maxCount", required = false, defaultValue = "1") int maxCount,
                                          @RequestParam(value = "minRate", required = false, defaultValue = "1") double minRate) {
        return torrentDownloadService.download(genres, minSeeds, maxCount, minRate);
    }

}
