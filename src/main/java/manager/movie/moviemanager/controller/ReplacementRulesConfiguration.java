package manager.movie.moviemanager.controller;

import lombok.AllArgsConstructor;
import manager.movie.moviemanager.configuration.NameScannerConfiguration;
import manager.movie.moviemanager.scanner.phrase.ReplacementRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
@Configuration
public class ReplacementRulesConfiguration {

    private final NameScannerConfiguration nameScannerConfiguration;

    @Bean
    public List<ReplacementRule> replacementRules() {
        return Stream.of(standardRules(), excludedPhrases())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

    }

    private List<ReplacementRule> excludedPhrases() {
        return nameScannerConfiguration.getExcludeNamePhrases()
                .stream()
                .map(String::toLowerCase)
                .map(ReplacementRule::new)
                .collect(Collectors.toList());
    }

    private List<ReplacementRule> standardRules() {
        return Arrays.asList(
                new ReplacementRule("(\\(.*?\\))"),
                new ReplacementRule("(\\[.*?\\])"),
                new ReplacementRule("\\.", " "),
                new ReplacementRule("[\\(\\)\\[\\]]"),
                new ReplacementRule("-", " "),
                new ReplacementRule(" {2,}", " ")
        );
    }

}
