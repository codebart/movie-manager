package manager.movie.moviemanager.utils;

import java.io.IOException;

@FunctionalInterface
public interface Timed<T> {

    T get() throws IOException;

}
