package manager.movie.moviemanager.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

@Slf4j
@UtilityClass
public class TimerUtils {

    public <T> T measured(Timed<T> supplier) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        T ret;
        try {
            ret = supplier.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        stopWatch.stop();
        log.debug("Measured duration: {} ms", stopWatch.getTotalTimeMillis());
        return ret;
    }

}
