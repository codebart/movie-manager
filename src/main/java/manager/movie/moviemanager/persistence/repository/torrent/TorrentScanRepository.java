package manager.movie.moviemanager.persistence.repository.torrent;

import manager.movie.moviemanager.persistence.model.torrent.TorrentScan;
import manager.movie.moviemanager.scanner.torrent.TorrentProvider;
import manager.movie.moviemanager.scanner.torrent.TorrentQuality;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TorrentScanRepository extends Neo4jRepository<TorrentScan, Long> {

    @Query("MATCH (scan:TorrentScan {year:{year}, page:{page}, provider:{provider}, quality:{quality}}) RETURN COUNT(scan) > 0")
    boolean scanExists(@Param("year") int year, @Param("page") int page, @Param("provider") TorrentProvider provider, @Param("quality") TorrentQuality quality);

}
