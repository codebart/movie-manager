package manager.movie.moviemanager.persistence.repository;

import manager.movie.moviemanager.persistence.model.Rate;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RateRepository extends Neo4jRepository<Rate, Long> {

    @Query("MATCH (rate:Rate)<-[:MOVIE_RATE]-(movie:Movie) WHERE ID(movie) = {movieId} RETURN rate")
    Optional<Rate> getMovieRate(@Param("movieId") Long movieId);

    Optional<Rate> findByRate(String rate);

}
