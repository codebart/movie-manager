package manager.movie.moviemanager.persistence.repository;

import manager.movie.moviemanager.persistence.model.Person;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface PersonRepository extends Neo4jRepository<Person, Long> {

    @Query("MATCH (actor:Person)-[:PLAYED_IN]->(movie:Movie) WHERE ID(movie) = {movieId} RETURN actor")
    Set<Person> getMovieActors(@Param("movieId") long movieId);

    @Query("MATCH (director:Person)-[:DIRECTED]->(movie:Movie) WHERE ID(movie) = {movieId} RETURN director")
    Set<Person> getMovieDirectors(@Param("movieId") long movieId);

    Optional<Person> getByName(String name);

}
