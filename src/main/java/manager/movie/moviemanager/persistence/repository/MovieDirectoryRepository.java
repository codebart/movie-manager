package manager.movie.moviemanager.persistence.repository;

import manager.movie.moviemanager.persistence.model.MovieDirectory;
import manager.movie.moviemanager.persistence.repository.result.MovieAndDirectory;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieDirectoryRepository extends Neo4jRepository<MovieDirectory, Long> {

    @Query("MATCH (directory:MovieDirectory {path:{path}}) RETURN COUNT(directory) > 0")
    boolean existsByPath(@Param("path") String path);

    @Query("MATCH (directory:MovieDirectory)-[:MOVIE_DIRECTORY]->(movie:Movie) RETURN movie, directory.path AS path")
    List<MovieAndDirectory> getAllMoviesWithDirectories();

    @Query("MATCH (directory:MovieDirectory)-[:MOVIE_DIRECTORY]->(movie:Movie) WHERE ID(movie)={movieId} RETURN COUNT(directory) > 0")
    boolean existsByMovie(@Param("movieId") long movieId);

}
