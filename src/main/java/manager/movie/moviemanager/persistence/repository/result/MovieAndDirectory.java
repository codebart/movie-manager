package manager.movie.moviemanager.persistence.repository.result;

import lombok.Data;
import manager.movie.moviemanager.persistence.model.Movie;
import org.springframework.data.neo4j.annotation.QueryResult;

@Data
@QueryResult
public class MovieAndDirectory {

    private Movie movie;
    private String path;

}