package manager.movie.moviemanager.persistence.repository;

import manager.movie.moviemanager.persistence.model.Movie;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends Neo4jRepository<Movie, Long> {

    @Query("MATCH (movie:Movie {extractedName:{name}, year:{year}}) RETURN COUNT(movie) > 0")
    boolean existsByExtractedNameAndYear(@Param("name") String name, @Param("year") int year);

    @Query("MATCH (movie:Movie {name:{name}, year:{year}}) RETURN COUNT(movie) > 0")
    boolean existsByNameAndYear(@Param("name") String name, @Param("year") int year);

    Movie findByExtractedNameAndYear(String extractedName, int year);

    Movie findByNameAndYear(String name, int year);

}
