package manager.movie.moviemanager.persistence.repository;

import manager.movie.moviemanager.persistence.model.Genre;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface GenreRepository extends Neo4jRepository<Genre, Long> {

    @Query("MATCH (genre:Genre)<-[:MOVIE_GENRE]-(movie:Movie) WHERE ID(movie) = {movieId} RETURN genre")
    Set<Genre> findMovieGenres(@Param("movieId") long movieId);

    Optional<Genre> findByName(String name);

}
