package manager.movie.moviemanager.persistence.repository.torrent;

import manager.movie.moviemanager.persistence.model.torrent.Torrent;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TorrentRepository extends Neo4jRepository<Torrent, Long> {

    List<Torrent> findAllByDownloadedIsFalse();

    @Query("MATCH (torrent:Torrent)-[:MOVIE_TORRENT]->(movie:Movie) WHERE ID(movie)={movieId} RETURN COUNT(torrent) > 0")
    boolean existsByMovie(@Param("movieId") long id);

    @Query("MATCH (torrent:Torrent)-[:MOVIE_TORRENT]->(movie:Movie), (movie)-[:MOVIE_GENRE]->(genre:Genre) WHERE ID(torrent) = {id} AND genre.name = {genre} RETURN count(genre) > 0")
    boolean hasTorrentMovieGenre(@Param("id") long id, @Param("genre") String genre);

}
