package manager.movie.moviemanager.persistence.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Required;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@NodeEntity
public abstract class AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Required
    private LocalDateTime creationTime = LocalDateTime.now();

}
