package manager.movie.moviemanager.persistence.model;

import lombok.*;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.Required;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@NodeEntity
public class MovieDirectory extends AbstractEntity {

    @Required
    @Relationship(type = "MOVIE_DIRECTORY")
    private Movie movie;

    @Required
    private String path;

}
