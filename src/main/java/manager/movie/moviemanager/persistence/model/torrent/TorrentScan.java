package manager.movie.moviemanager.persistence.model.torrent;


import lombok.*;
import manager.movie.moviemanager.persistence.model.AbstractEntity;
import manager.movie.moviemanager.scanner.torrent.TorrentProvider;
import manager.movie.moviemanager.scanner.torrent.TorrentQuality;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Required;
import org.neo4j.ogm.annotation.typeconversion.EnumString;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@NodeEntity
public class TorrentScan extends AbstractEntity {

    @Required
    private String link;

    @Required
    private Integer year;

    @Required
    private Integer page;

    @Required
    private TorrentQuality quality;

    @EnumString(TorrentProvider.class)
    private TorrentProvider provider;

}
