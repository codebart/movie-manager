package manager.movie.moviemanager.persistence.model;

import lombok.*;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.Required;

import java.util.HashSet;
import java.util.Set;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@NodeEntity
public class Movie extends AbstractEntity {

    @Required
    private String name;

    @Required
    private Integer year;

    @Builder.Default
    @Relationship(type = "MOVIE_GENRE")
    private Set<Genre> genres = new HashSet<>();

    @Builder.Default
    @Relationship(type = "PLAYED_IN", direction = Relationship.INCOMING)
    private Set<Person> actors = new HashSet<>();

    @Builder.Default
    @Relationship(type = "DIRECTED", direction = Relationship.INCOMING)
    private Set<Person> directors = new HashSet<>();

    @Relationship(type = "MOVIE_RATE")
    private Rate rate;

    private String extractedName;

    @Index(unique = true)
    private String imdbId;

}
