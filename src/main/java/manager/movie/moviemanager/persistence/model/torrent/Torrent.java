package manager.movie.moviemanager.persistence.model.torrent;

import lombok.*;
import manager.movie.moviemanager.persistence.model.AbstractEntity;
import manager.movie.moviemanager.persistence.model.Movie;
import manager.movie.moviemanager.scanner.torrent.TorrentProvider;
import manager.movie.moviemanager.scanner.torrent.TorrentQuality;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.Required;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@NodeEntity
public class Torrent extends AbstractEntity {

    @Required
    private Integer year;

    @Required
    private String name;

    @Required
    private String link;

    @Required
    private String magnet;

    @Builder.Default
    @Required
    private Boolean downloaded = false;

    @Required
    private TorrentQuality quality;

    @Required
    private TorrentProvider provider;

    @Relationship(type = "SCANNED_BY")
    private TorrentScan scan;

    @Relationship(type = "MOVIE_TORRENT")
    private Movie movie;

    private Integer seeds;
    private Integer leech;
    private String details;
    private String originalSize;
    private Integer sizeInMb;

}
