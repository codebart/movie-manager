package manager.movie.moviemanager.configuration;

import lombok.Data;
import manager.movie.moviemanager.scanner.torrent.TorrentQuality;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.awt.*;
import java.time.LocalDate;

@Data
@Configuration
@PropertySource(value = "classpath:torrent-configuration.yaml", factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties(prefix = "torrent")
public class TorrentConfiguration {

    private Integer sizeFrom = 1000;
    private Integer sizeTo = 7000;
    private Integer yearFrom = 1960;
    private Integer yearTo = LocalDate.now().getYear();
    private TorrentQuality torrentQuality = TorrentQuality.FULL_HD;
    private Integer pageLimit = 20;
    private Integer requestTimeout = 30 * 1000;
    private Integer minSeeds = 3;
    private Integer requestInterval = 3 * 1000;

    @Bean
    @ConditionalOnExpression(value = "T(java.awt.Desktop).isDesktopSupported()")
    public Desktop getDesktop() {
        return Desktop.getDesktop();
    }

}
