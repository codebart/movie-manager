package manager.movie.moviemanager.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

@Data
@Configuration
@PropertySource(value = "classpath:name-scanner-configuration.yaml", factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties(prefix = "name-scanner")
public class NameScannerConfiguration {

    private List<String> excludeNamePhrases = new ArrayList<>();

}
