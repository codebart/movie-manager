package manager.movie.moviemanager.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

@Data
@Configuration
@PropertySource(value = "classpath:configuration.yaml", factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties(prefix = "app")
public class ApplicationConfiguration {

    private String linksDirectory;
    private Integer scanDepth = 1;
    private List<String> ignoredDirectories = new ArrayList<>();
    private List<String> searchDirectories = new ArrayList<>();

}
