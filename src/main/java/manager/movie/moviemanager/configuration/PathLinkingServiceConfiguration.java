package manager.movie.moviemanager.configuration;

import com.sun.javafx.PlatformUtil;
import manager.movie.moviemanager.linking.PathSanitizer;
import manager.movie.moviemanager.linking.WindowsPathSanitizer;
import manager.movie.moviemanager.linking.service.PathLinkingService;
import manager.movie.moviemanager.linking.service.WindowsLinkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PathLinkingServiceConfiguration {

    private final ApplicationConfiguration applicationConfiguration;

    @Autowired
    public PathLinkingServiceConfiguration(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
    }

    @Bean
    public PathLinkingService getPathLinkingService() {
        if (PlatformUtil.isWindows()) {
            return new WindowsLinkingService(applicationConfiguration, getPathSanitizer());
        }
        throw new IllegalStateException("System not recognized");
    }

    @Bean
    public PathSanitizer getPathSanitizer() {
        if (PlatformUtil.isWindows()) {
            return new WindowsPathSanitizer();
        }
        throw new IllegalStateException("System not recognized");
    }

}
