package manager.movie.moviemanager.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@PropertySource(value = "classpath:omdb-configuration.yaml", factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties(prefix = "omdb")
public class OmdbProperties {

    private String apiKey;

}
