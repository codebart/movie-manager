package manager.movie.moviemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@EnableNeo4jRepositories
@EnableConfigurationProperties
@SpringBootApplication
public class MovieManagerApplication {

	public static void main(String[] args) {
//		args = new String[]{"--download-torrents=true", "--torrent-min-seeds=30", "--torrent-count=80", "--torrent-rate=5.0"};
//		args = new String[] {"--scan=true"};
		SpringApplication.run(MovieManagerApplication.class, args);
	}
}
